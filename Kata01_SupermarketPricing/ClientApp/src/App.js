﻿import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { ListItems } from './components/ListItems';

import './custom.css'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={ListItems} />
                <Route path='/list-items' component={ListItems} />
            </Layout>
        );
    }
}
