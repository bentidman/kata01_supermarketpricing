﻿import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import Button from 'react-bootstrap/Button'


const AddItemForm = (props) => {

    const formik = useFormik({
        initialValues: {
            upc: '',
            name: '',
            price: '',
        },
        validationSchema: Yup.object({
            upc: Yup.string()
                .length(12, 'Must be 12 characters')
                .required('Required'),
            name: Yup.string().required('Required'),
            price: Yup.number()
                .typeError('Must be a number')
                .moreThan(0.09, 'Price must be $0.10 or more')
                .required('Required'),
        }),
        onSubmit: async (values, actions) => {
            let status = await props.onSubmit(values);

            if (!status.success) {
                actions.setFieldError('general', status.errorMessage);
            }
            else {
                values.upc = '';
                values.name = '';
                values.price = '';
            }
        },
    });

    return (
        <form onSubmit={formik.handleSubmit} autocomplete="off">
            <div className="mb-3">
                <div className="row">
                    <div className="col">
                        <div className="input-group has-validation">
                            <input className="form-control"
                                id="upc"
                                type="text"
                                data-lpignore="true"
                                placeholder="UPC"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.upc} />
                            {formik.touched.upc && formik.errors.upc ? <div className="validation-error">{formik.errors.upc}</div> : null}
                        </div>
                    </div>
                    <div className="col">
                        <div className="input-group has-validation">
                            <input className="form-control"
                                id="name"
                                type="text"
                                data-lpignore="true"
                                placeholder="Name"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.name} />
                            {formik.touched.name && formik.errors.name ? <div className="validation-error">{formik.errors.name}</div> : null}
                        </div>
                    </div>
                    <div className="col">
                        <div className="input-group has-validation">
                            <input className="form-control"
                                id="price"
                                type="text"
                                data-lpignore="true"
                                placeholder="Price"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.price} />
                            {formik.touched.price && formik.errors.price ? <div className="validation-error">{formik.errors.price}</div> : null}
                        </div>
                    </div>
                    <div className="col">
                        <Button type="submit" variant="primary">Save</Button>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        {formik.errors.general ? <div className="validation-error">{formik.errors.general}</div> : null}
                    </div>
                </div>
            </div>
        </form >
    );
};

export default AddItemForm;