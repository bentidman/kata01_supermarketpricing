﻿import React, { Component } from 'react';
import { ListItemsTable } from './ListItemsTable';
import AddItemForm from './AddItemForm';

export class ListItems extends Component {

    constructor(props) {
        super(props);
        this.state = { items: [], loading: true };
        this.saveItem = this.saveItem.bind(this);
    }

    componentDidMount() {
        this.populateItemData();
    }

    async populateItemData() {
        const response = await fetch('/api/supermarketItem');
        const data = await response.json();
        this.setState({ items: data, loading: false });
    }

    checkResponseStatus(res) {
        if (res.ok) {
            return res
        } else {
            throw new Error("Error Saving Item! Maybe Ben can fix it... :)");
        }
    }

    async saveItem(item) {
        var status = {
            success: false,
            errorMessage: ''
        };

        try {
            const response = await fetch('/api/SupermarketItem', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(item)
            });
            this.checkResponseStatus(response);
            status.success = true;
            this.populateItemData();
        }
        catch (error) {
            status.success = false;
            status.errorMessage = error.message;
        }
        return status;
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : <ListItemsTable items={this.state.items} />;

        return (
            <div>
                <h1 id="tabelLabel">Items</h1>
                <AddItemForm onSubmit={this.saveItem}/>
                {contents}
            </div>
        );
    }
}
