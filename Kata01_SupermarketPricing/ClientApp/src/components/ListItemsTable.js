﻿import React, { Component } from 'react';

export class ListItemsTable extends Component {
    static displayName = ListItemsTable.name;

    constructor(props) {
        super(props);
        this.state = {loading: true };
    }

    render() {
        return (
            <div>
                <table className='table table-striped' aria-labelledby="tabelLabel">
                    <thead>
                        <tr>
                            <th>UPC</th>
                            <th>Name</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.items.map(item =>
                            <tr key={item.id}>
                                <td>{item.upc}</td>
                                <td>{item.name}</td>
                                <td>${item.price.toFixed(2).toString()}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }
}
