using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kata01_SupermarketPricing.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Kata01_SupermarketPricing.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupermarketItemController : ControllerBase
    {
        private readonly SupermarketContext _context;

        public SupermarketItemController(SupermarketContext context)
        {
            _context = context;
        }

        // GET: api/SupermarkerItem
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SupermarketItem>>> GetItems()
        {
            var items = await _context.Items.ToListAsync();

            return items;
        }

        // GET: api/SupermarkerItem/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SupermarketItem>> GetSupermarketItem(int id)
        {
            var supermarketItem = await _context.Items.FindAsync(id);

            if (supermarketItem == null)
            {
                return NotFound();
            }

            return supermarketItem;
        }

        // PUT: api/SupermarkerItem/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSupermarketItem(int id, SupermarketItem supermarketItem)
        {
            if (id != supermarketItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(supermarketItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SupermarketItemExists(id))
                {
                    return NotFound();
                }
                else 
                {    
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SupermarkerItem
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SupermarketItem>> PostSupermarketItem(SupermarketItem supermarketItem)
        {
            _context.Items.Add(supermarketItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSupermarketItem", new { id = supermarketItem.Id }, supermarketItem);
        }

        // DELETE: api/SupermarkerItem/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSupermarketItem(int id)
        {
            var supermarketItem = await _context.Items.FindAsync(id);
            if (supermarketItem == null)
            {
                return NotFound();
            }

            _context.Items.Remove(supermarketItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SupermarketItemExists(int id)
        {
            return _context.Items.Any(e => e.Id == id);
        }
    }
}
