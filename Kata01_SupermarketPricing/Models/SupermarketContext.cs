﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Kata01_SupermarketPricing.Models
{
    public class SupermarketContext : DbContext
    {
        public DbSet<SupermarketItem> Items { get; set; }

        public SupermarketContext(DbContextOptions<SupermarketContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SupermarketItem>().HasData(new SupermarketItem {
                Id = 1,
                Name = "Bagel Bites",
                Price = 1.00,
                UPC = "1234"
            }) ;
        }
    }
}
