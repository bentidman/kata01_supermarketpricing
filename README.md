﻿# About
This project was created to show/practice full stack coding skills using dotnet and React. Loosely based off of [Kata01: Supermarket Pricing](http://codekata.com/kata/kata01-supermarket-pricing/)

# Important Dependencies

 - [dotnet core 5.0](https://dotnet.microsoft.com/download/dotnet)
 - [dotnet Entity Framework CLI](https://docs.microsoft.com/en-us/ef/core/cli/dotnet)
 - [Visual Studio 2019](https://visualstudio.microsoft.com/downloads/)
 
# Instructions to Run 
 - Clone the repository.
 - In the terminal/console navigate to the solution directory and run `dotnet ef database update`. This should create your sqllite database file and update it's schema.
 - Run within visual studio 2019 
